<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Online Test Plus Website</title>
<link href="style/style.css" rel="stylesheet" type="text/css" />
<link href="style/follow_us.css" rel="stylesheet" type="text/css" />
<link href="style/moun_top.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Poiret+One|Montserrat+Alternates|Cinzel+Decorative' rel='stylesheet' type='text/css'> <!--font-family: 'Poiret One', cursive;
font-family: 'Montserrat Alternates', sans-serif;
font-family: 'Cinzel Decorative', cursive; -->
<!--Bubble _content -->
<link rel="stylesheet" href="Bubble_content/css/Bubble_style.css" type="text/css" media="screen"/>
  <script type="text/javascript" src="Bubble_content/jquery.min.js"></script>
        <script type="text/javascript" src="Bubble_content/jquery.easing.1.3.js"></script>
        <script type="text/javascript">
            $(function() {
                $('#nav > div').hover(
                function () {
                    var $this = $(this);
                    $this.find('img').stop().animate({
                        'width'     :'140px',
                        'height'    :'140px',
                        'top'       :'-40px',
                        'left'      :'-43px',
                        'opacity'   :'1.0'
                    },500,'easeOutBack',function(){
                        $(this).parent().find('ul').fadeIn(700);
                    });

                    $this.find('a:first,h2').addClass('active');
                },
                function () {
                    var $this = $(this);
                    $this.find('ul').fadeOut(500);
                    $this.find('img').stop().animate({
                        'width'     :'52px',
                        'height'    :'52px',
                        'top'       :'0px',
                        'left'      :'0px',
                        'opacity'   :'0.1'
                    },5000,'easeOutBack');

                    $this.find('a:first,h2').removeClass('active');
                }
            );
            });
        </script>
        
              <script type="text/javascript">
		function showContent(id){
				
				$("#DEFAULT_ID").slideUp();
				$("#Content1_ID").slideUp();
				$("#Content2_ID").slideUp();
				$("#Content3_ID").slideUp();
				$("#Content4_ID").slideUp();
				$("#Content5_ID").slideUp();
				$("#Content6_ID").slideUp();
				$("#Content7_ID").slideUp();
				$("#Content8_ID").slideUp();
				if(id=="MENU1_ID"){
					$("#Content1_ID").slideDown();
				}
				if(id=="MENU2_ID"){
					$("#Content2_ID").slideDown();
				}
				if(id=="MENU3_ID"){
					$("#Content3_ID").slideDown();
				}
				if(id=="MENU4_ID"){
					$("#Content4_ID").slideDown();
				}
				if(id=="MENU5_ID"){
					$("#Content5_ID").slideDown();
				}
				if(id=="MENU6_ID"){
					$("#Content6_ID").slideDown();
				}
				if(id=="MENU7_ID"){
					$("#Content7_ID").slideDown();
				}
				if(id=="MENU8_ID"){
					$("#Content8_ID").slideDown();
				}
			}
			$(document).ready(function(){
				$("#comment").hide();
				$("#Content1_ID").slideUp();
				$("#Content2_ID").slideUp();
				$("#Content3_ID").slideUp();
				$("#Content4_ID").slideUp();
				$("#Content5_ID").slideUp();
				$("#Content6_ID").slideUp();
				$("#Content7_ID").slideUp();
				$("#Content8_ID").slideUp();
				});
			
		</script>
        
        <!--Enquiry_form -->
        <link rel="stylesheet" href="Enquiry_form/Enquiry_style.css" type="text/css" media="screen"/>
        <script type="text/javascript">
			function showTextBox(box){
				g = document.getElementById(box);
				if(g.text == "more"){
					g.text="less";
					$("#"+box).text("less");
					$("#comment").slideDown();
				}else{
					g.text="more";
					$("#"+box).text("more");
					$("#comment").slideUp();
				}
			}
			
		</script>
        
        <script type="text/javascript">
		      function overlayHide() {
                  el = document.getElementById("overlay");
                  //el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
                  el.style.visibility =  "hidden" ;
              }

              function overlayShow() {
                  el = document.getElementById("overlay");
                  el.style.visibility =  "visible";
			  }
        </script>
 <!--Snapshots -->
 <link rel="stylesheet" type="text/css" href="Screen_Shots/reset-min.css">
<link href='Screen_Shots/tabulous.css' rel='stylesheet' type='text/css'>
<link href="Screen_Shots/jquerysctipttop.css" rel="stylesheet" type="text/css" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script> 
<script type="text/javascript" src="Screen_Shots/tabulous.js"></script> 
<script type="text/javascript" src="Screen_Shots/js.js"></script>
 
</head>

<body>
	<div class="main_division">
		<div class="header_main">
			<div class="header">
				<div class="logo"><a href="index.html"><img src="images/logo.png"></a></div>
<div class="follow_us">

<ul>

<li><a class="entypo-linkedin-circled" title="Linkedin" href="http://in.linkedin.com" target="_new"></a></li>
<li><a class="entypo-skype-circled" title="Skype" href="http://www.skype.com/en/" target="_new"></a></li>
<li><a class="entypo-gplus-circled" title="Google Plus" href="http://plus.google.com" target="_new"></a></li>
<li><a class="entypo-facebook-circled" title="Facebook" href="http://www.facebook.com" target="_new"></a></li>

</ul>


</div>

<div class="menu">
<nav id="menu">
  <ul>
    <li>
      <a href="#">HOME</a>
    </li>
    <li>
      <a href="#">Features</a>  
    </li>
    <li>
      <a href="#">Screenshots</a>
    </li>
    <li>
      <a href="#">Online Demo</a>
    </li>
    <li>
      <a href="#">View it in Action</a>
    </li>
    <li>
      <a href="#" class="yellow">CONTACT</a>
    </li>
  </ul>
</nav>
</div>

</div>
			</div>
			
            <div class="content_main">
				<div class="content">
					<div style="width:830px; height:600px; float:left; ">
						<iframe src="Untitled-1.html" width="100%" height="100%" scrolling="no" marginheight="0" marginwidth="0" frameborder="0">
                        </iframe>
                    </div>
					
                    <div style="float: left; height:645px; margin-left: 20px; width:340px;">
						<div id="form-div">
							<h4 style="font-family: 'Cinzel Decorative', cursive; font-size:30px; margin-bottom:10px;">Enquiry</h4>
							<form class="form" id="form1" name="form" action="send_mail.php" method="post">
      
      <p class="name">
        <input name="name" type="text" pattern="[a-z || A-Z]+" required title="Please Enter Correct Name" class="validate[required,custom[onlyLetter],length[0,100]] feedback-input" placeholder="Name" id="name" />
      </p>
      
      <p class="email">
        <input name="email" type="email" required class="validate[required,custom[email]] feedback-input" id="email" placeholder="Email" />
      </p>
      <p class="Phone">
        <input name="phone" type="text" required title="Please Enter Correct Numbers" pattern="[0-9]+" class="validate[required,custom[Phone]] feedback-input" id="Phone" placeholder="Phone" />
      </p>
      
    <p class="text">
        <textarea name="comment" class="validate[required,length[6,300]] feedback-input" id="comment" placeholder="Comment"></textarea>
      </p>
     
      
     <div class="submit">
     <input style="float:right; margin-right:10px; margin-top:10px;  border:none; text-decoration:none; background-color:#A6BC50; cursor:pointer; color:#000;" type="submit" name="submit" value="send" />
     <a style="float:right; margin-right:10px; margin-top:10px;  border:none;  cursor:pointer; color:#000;"  id="TextAreaId" onclick="showTextBox(this.id);">more</a>
       
        
      </div>
    </form>
                            <div style="width:430px; height:315px;margin-top:30px; float:left; " >
                        
                         
                        <div id="tabs">
<ul>
<li><a href="#tabs-1">Snapshots</a></li>
<li><a href="#tabs-2">Test</a></li>
</ul>
<div id="tabs_container" style=" height:300px; ">
<div id="tabs-1" onclick="overlayShow();">
<p style="width:430px; height:315px;"><iframe src="Screen_Shots/Snapshots_Slider/index.html" width="100%" height="100%" scrolling="no" marginheight="0" marginwidth="0" frameborder="0"></iframe></p>
</div>
<div id="tabs-2" onclick="overlayShow();">
<p style="background-color:#F0F;width:340px; height:200px;"></p>
</div>
</div>
</div>
                        </div>
						</div>
					</div>
				</div>
			</div>
		</div><!--wrapper-->
		
        <div id="overlay">

	<div class="innerDiv" style="width:750px;height:445px; background-color:#FFF;  webkit-border-radius: 20px;-moz-border-radius: 20px;border-radius: 20px;

    
    ">
     	<img src="images/Delete16.png" height="16px" width="16px" style="float:right; margin:3px;" onclick="overlayHide();" />
    	<iframe src="Screen_Shots_Zoom/index.html" width="100%" height="100%" scrolling="no" marginheight="0" marginwidth="0" frameborder="0">
        </iframe> 
	</div> 
	
    </div>
	
</body>
</html>
